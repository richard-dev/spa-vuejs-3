import { createRouter, createWebHistory } from 'vue-router';

// Importa los componentes Vue utilizados en las rutas
import InicioPage from "@/components/InicioPage";
import NosotrosPage from "@/components/NosotrosPage";

// Define las rutas de la aplicación
const routes = [
    {
        path: '/',            // Ruta URL
        name: 'Inicio',         // Nombre de la ruta
        component: InicioPage,  // Componente asociado a la ruta
    },
    {
        path: '/nosotros',       // Ruta URL
        name: 'Nosotros',        // Nombre de la ruta
        component: NosotrosPage, // Componente asociado a la ruta
    }
];

// Crea un enrutador Vue utilizando las rutas y el historial basado en hash
const router = createRouter({
    history: createWebHistory(),    // Usa el historial basado en hash para las rutas
    routes                          // Configuración de las rutas definidas anteriormente
});

// Exporta el enrutador para que pueda ser importado por 'main.js'
export default router;