import { createApp } from 'vue'

// Importar Boostrap
import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap"

// Importa el enrutador creado en el archivo 'router.js'
import router from './router';

// Importa el componente principal de la aplicación
import App from './App.vue';

// Crea una instancia de la aplicación Vue principal
const app = createApp(App);

// Usa el enrutador Vue en la instancia de la aplicación
app.use(router);

// Cambiar el titulo dinamicamente con una constante en el archivo .env del proyecto
router.beforeEach((to, from, next) => {
    document.title = `${ to.name } - ${ process.env.VUE_APP_TITLE }`;
    next();
});

// Monta la aplicación Vue en el elemento HTML con el ID 'app'
app.mount('#app');
